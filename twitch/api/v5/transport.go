package v5

import (
	"io"
	"net/http"
	"net/url"
	"sync"
)

// transport is a wrapper transport for communicating with the Twitch API.
type transport struct {
	base             http.RoundTripper
	lock             sync.Mutex
	clientID         string
	modifiedRequests map[*http.Request]*http.Request
}

func (t *transport) RoundTrip(request *http.Request) (*http.Response, error) {
	modifiedRequest := t.cloneRequest(request) // per RoundTripper contract
	modifiedRequest.Header.Add("Accept", "application/vnd.twitchtv.v5+json")
	modifiedRequest.Header.Add("Client-ID", t.clientID)
	apiURL, err := url.Parse("https://api.twitch.tv/kraken/")
	if err != nil {
		return nil, err
	}
	modifiedRequest.URL = apiURL.ResolveReference(request.URL)
	t.setModifiedRequest(request, modifiedRequest)
	response, err := t.base.RoundTrip(modifiedRequest)
	if err != nil {
		t.setModifiedRequest(request, nil)
		return nil, err
	}
	response.Body = &onEOFReader{
		readCloser: response.Body,
		cleanup:    func() { t.setModifiedRequest(request, nil) },
	}
	return response, nil
}

// CancelRequest cancels an in-flight request by closing its connection.
func (t *transport) CancelRequest(request *http.Request) {
	if canceler, ok := t.base.(interface {
		CancelRequest(*http.Request)
	}); ok {
		t.lock.Lock()
		modifiedRequest := t.modifiedRequests[request]
		delete(t.modifiedRequests, request)
		t.lock.Unlock()
		canceler.CancelRequest(modifiedRequest)
	}
}

// cloneRequest returns a clone of the provided *http.Request.
// The clone is a shallow copy of the struct and its Header map.
func (t *transport) cloneRequest(request *http.Request) *http.Request {
	// shallow copy of the struct
	modifiedRequest := new(http.Request)
	*modifiedRequest = *request
	// deep copy of the Header
	modifiedRequest.Header = make(http.Header, len(request.Header))
	for key, values := range request.Header {
		modifiedRequest.Header[key] = append([]string(nil), values...)
	}

	return modifiedRequest
}

func (t *transport) setModifiedRequest(originalRequest, modifiedRequest *http.Request) {
	t.lock.Lock()
	defer t.lock.Unlock()
	if t.modifiedRequests == nil {
		t.modifiedRequests = make(map[*http.Request]*http.Request)
	}
	if modifiedRequest == nil {
		delete(t.modifiedRequests, originalRequest)
	} else {
		t.modifiedRequests[originalRequest] = modifiedRequest
	}
}

type onEOFReader struct {
	readCloser io.ReadCloser
	cleanup    func()
}

func (r *onEOFReader) Read(p []byte) (n int, err error) {
	n, err = r.readCloser.Read(p)
	if err == io.EOF {
		r.runFunc()
	}
	return
}

func (r *onEOFReader) Close() error {
	err := r.readCloser.Close()
	r.runFunc()
	return err
}

func (r *onEOFReader) runFunc() {
	if cleanup := r.cleanup; cleanup != nil {
		cleanup()
		r.cleanup = nil
	}
}
