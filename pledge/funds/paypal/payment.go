package paypal

import (
	"fmt"
	"sync"

	"github.com/logpacker/PayPal-Go-SDK"
	"github.com/shopspring/decimal"
	"github.com/sirupsen/logrus"
)

// Payment represents a payment using PayPal
type Payment struct {
	sync.Mutex
	payment  *paypalsdk.Payment
	sale     *paypalsdk.Sale
	consumed decimal.Decimal
	client   *paypalsdk.Client
}

// maximum returns the maximum amount of the approved pledge.
func (p *Payment) maximum() (maximum decimal.Decimal, err error) {
	maximum, err = decimal.NewFromString(p.sale.Amount.Total)
	if err != nil {
		logrus.Errorf("Could not parse authorized amount (%s) from PayPal sale: %s", p.sale.Amount.Total, err)
	}
	return
}

// Capture captures the specified amount of money from the total authorized.
func (p *Payment) Capture(amount decimal.Decimal) error {
	p.Lock()
	defer p.Unlock()

	if p.payment != nil {
		executedPayment, err := p.client.ExecuteApprovedPayment(p.payment.ID, p.payment.Payer.PayerInfo.PayerID)
		if err != nil {
			logrus.Errorf("Failed to execute the approved payment: %s", err)
			return err
		}
		p.payment, p.sale = nil, executedPayment.Transactions[0].RelatedResources[0].Sale
	} else if p.sale == nil {
		panic("PayPal payment has neither a sale nor a payment")
	}

	maximum, err := p.maximum()
	if err != nil {
		return err
	}
	if p.consumed.Add(amount).Cmp(maximum) > 0 {
		return fmt.Errorf("Pledge has reached its maximum of $%s", maximum.StringFixed(2))
	}
	p.consumed = p.consumed.Add(amount)
	return nil
}

// Finalize finalizes the transaction, taking care of any cleanup.
func (p *Payment) Finalize() error {
	p.Lock()
	defer p.Unlock()

	if p.sale == nil {
		return nil
	}

	maximum, err := p.maximum()
	if err != nil {
		return err
	}
	switch p.consumed.Cmp(maximum) {
	// We've used the whole authorized amount, so there is no refund.
	case 0:
	// We've somehow consumed more than the maximum. This should never happen.
	case 1:
		logrus.Fatalf("Payment captured %s which is more than the authorized amount %s", p.consumed, maximum)
		return fmt.Errorf("Payment captured %s which is more than the authorized amount %s", p.consumed, maximum)
	// The full amount was not used. Refund the difference if the payment was
	// executed.
	case -1:
		if p.sale != nil {
			if err != nil {
				logrus.Errorf("Could not parse transaction fee (%s) from PayPal sale: %s", p.sale.TransactionFee.Value, err)
				return err
			}
			_, err = p.client.RefundSale(p.sale.ID, &paypalsdk.Amount{
				Total:    maximum.Sub(p.consumed).StringFixed(2),
				Currency: "USD",
			})
			if err != nil {
				logrus.Errorf("Failed to refund unused funds: %s", err)
				return err
			}
		} else {
			// TODO (zeffron 2017-04-16) Figure out how to void the authorized but not executed sale.
		}
	}
	return nil
}

// Amount returns the amount of money actually spent through this payment.
func (p *Payment) Amount() decimal.Decimal {
	return p.consumed
}
