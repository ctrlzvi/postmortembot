package funds

import "github.com/shopspring/decimal"

// Payment represents the funds set aside for a pledge.
type Payment interface {
	Capture(amount decimal.Decimal) error
	Finalize() error
	Amount() decimal.Decimal
}
