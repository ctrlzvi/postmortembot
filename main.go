package main

import (
	"context"
	"fmt"
	"net/http"
	"regexp"
	"time"

	"github.com/gorilla/mux"
	"github.com/shopspring/decimal"
	"github.com/sirupsen/logrus"
	"gitlab.com/ctrlzvi/postmortembot/pledge"
	"gitlab.com/ctrlzvi/postmortembot/pledge/funds"
	"gitlab.com/ctrlzvi/postmortembot/pledge/funds/paypal"
	"gitlab.com/ctrlzvi/postmortembot/streamlabs"
	"gitlab.com/ctrlzvi/postmortembot/streamlabs/api/v1"
	"gitlab.com/ctrlzvi/postmortembot/twitch"
)

func main() {
	bot := twitch.NewBot("postmortembot", "", "")
	v1Router := bot.Router().PathPrefix("/api/v1").Subrouter()
	streamlabsOauth2CSRFTokens := streamlabs.NewOauth2CSRFTokens()
	streamlabsAuthorizationBuilderRoute := v1Router.Host("{host:^.+$}").Path("/oauth2/streamlabs/authorize").BuildOnly()
	v1Router.Path("/oauth2/streamlabs/authorize").Queries("state", "{state}", "code", "{code}").MatcherFunc(
		func(r *http.Request, rm *mux.RouteMatch) bool {
			return streamlabsOauth2CSRFTokens.ConsumeToken(r.FormValue("state"))
		},
	).HandlerFunc(
		func(w http.ResponseWriter, r *http.Request) {
			// TODO (zeffron 2017-04-01) Make this not tied to streamlabs
			token, err := streamlabs.Token(r.Context(), streamlabsAuthorizationBuilderRoute, mux.Vars(r)["code"])
			if err != nil {
				w.WriteHeader(http.StatusUnauthorized)
				w.Write([]byte(err.Error()))
				return
			}
			config, err := streamlabs.Config(r.Context(), streamlabsAuthorizationBuilderRoute)
			if err != nil {
				w.WriteHeader(http.StatusInternalServerError)
				w.Write([]byte(err.Error()))
				return
			}
			user, err := (*v1.Client)(config.Client(r.Context(), token)).User()
			if err != nil {
				w.WriteHeader(http.StatusInternalServerError)
				w.Write([]byte(err.Error()))
				return
			}
			_, err = bot.RegisterBroadcaster(
				user.Name,
				(*v1.Client)(config.Client(context.Background(), token)),
			)
			if err != nil {
				w.WriteHeader(http.StatusInternalServerError)
				w.Write([]byte(err.Error()))
				return
			}

			w.WriteHeader(http.StatusOK)
			w.Write([]byte(fmt.Sprintf("Successfully registered %s", user.Name)))
		},
	)
	v1Router.Path("/register").HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		url, err := streamlabs.Authorize(r.Context(), streamlabsAuthorizationBuilderRoute, streamlabsOauth2CSRFTokens)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			w.Write([]byte(err.Error()))
			return
		}
		http.Redirect(w, r, url, http.StatusTemporaryRedirect)
	})

	paypalProvder, err := paypal.NewProvider(v1Router, "", "")
	if err != nil {
		logrus.Fatalf("Could not create the PayPal provider: %s", err)
		return
	}

	bot.RegisterCommand("rip", ripCommand)

	bot.RegisterCommand("reincarnations", func(command *twitch.Command, broadcaster *twitch.Broadcaster) {
		history := broadcaster.History(DeathEventType)
		broadcaster.Reply(command.User, fmt.Sprintf("%d deaths this session", len(history)))
	})

	bot.RegisterCommand("autopsy", autopsyCommand)

	bot.RegisterCommand("pledge", pledgeCommand(paypalProvder))

	bot.Run()
}

func ripCommand(command *twitch.Command, broadcaster *twitch.Broadcaster) {
	previousDeath := broadcaster.LastEvent(DeathEventType)
	if previousDeath != nil {
		const coalesceWindow = 90 * time.Second
		if command.Timestamp.Sub(previousDeath.Timestamp()) < coalesceWindow {
			previousDeath := previousDeath.(*Death)
			if previousDeath.Cause() == "" {
				previousDeath.UpdateCause(command.Arguments)
			}
			return
		}
	}
	pledges := broadcaster.Pledges(DeathEventType)
	total := decimal.Decimal{}
	donorsMap := map[string]interface{}{}
	for _, pledge := range pledges {
		if pledge.Consume() == nil {
			total = total.Add(pledge.Amount())
			donorsMap[pledge.Donor()] = nil
		}
	}
	broadcaster.AddEvent(NewDeath(command.Arguments, command.Timestamp))
	donors := make([]string, 0, len(donorsMap))
	for donor := range donorsMap {
		donors = append(donors, donor)
	}
	err := broadcaster.Donate(total, donors)
	if err != nil {
		logrus.Errorf("Failed to send donation to Streamlabs: %s", err)
	}
	broadcaster.Reply(command.User, fmt.Sprintf("Taken to soon. Rest in peace, %s.", broadcaster.DisplayName()))
}

func autopsyCommand(command *twitch.Command, broadcaster *twitch.Broadcaster) {
	event := broadcaster.LastEvent(DeathEventType)
	if event == nil {
		broadcaster.Reply(command.User, fmt.Sprintf("%s appears to be immortal this session", broadcaster.DisplayName()))
	} else {
		since := time.Time{}.Add(time.Since(event.Timestamp()))
		broadcaster.Reply(command.User, fmt.Sprintf("Last death was %d hours, %d minutes and %d seconds ago: %s", since.Hour(), since.Minute(), since.Second(), event.Note()))
	}
}

var pledgeRegexp = regexp.MustCompile(`^\s*\$?([\d.]+)\s+\$?([\d.]+)\s*$`)

func pledgeCommand(provider funds.Provider) func(command *twitch.Command, broadcaster *twitch.Broadcaster) {
	return func(command *twitch.Command, broadcaster *twitch.Broadcaster) {
		submatches := pledgeRegexp.FindStringSubmatch(command.Arguments)
		if submatches == nil {
			logrus.Errorf(`Invalid arguments to pledge command "%s"`, command.Arguments)
			return
		}

		amount, err := decimal.NewFromString(submatches[1])
		if err != nil {
			logrus.Errorf(`Could not convert amount "%s" into a decimal value: %s`, submatches[1], err)
			return
		}
		amount.Truncate(2)
		minimumPledge := decimal.New(5, -1)
		if amount.Cmp(minimumPledge) < 0 {
			broadcaster.Reply(command.User, fmt.Sprintf("Pledged amount must be at least $%s.", minimumPledge.StringFixed(2)))
			return
		}

		maximum, err := decimal.NewFromString(submatches[2])
		if err != nil {
			logrus.Errorf(`Could not convert maximum "%s" into a decimal value: %s`, submatches[2], err)
			return
		}
		maximum.Truncate(2)
		if maximum.Cmp(amount) < 0 {
			broadcaster.Reply(command.User, "Maximum amount must be at least the pledged amount.")
			return
		}

		pledge := pledge.NewPledge(broadcaster.DisplayName(), command.User, amount, maximum)
		authorizationURL, err := pledge.Fund(provider)
		if err != nil {
			logrus.Errorf("Failed to fund the pledge for %s for %s: %s", command.User, broadcaster.DisplayName(), err)
			return
		}
		broadcaster.PrivateReply(command.User, fmt.Sprintf("Please go to %s to approve the payment", authorizationURL))
		broadcaster.AddPledge(DeathEventType, pledge)
	}
}

type contextKey string

var commandKey = contextKey("command")
