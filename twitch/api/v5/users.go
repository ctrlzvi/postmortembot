package v5

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
	"strings"
	"time"
)

// User represents a Twitch user.
type User struct {
	ID               int64         `json:"_id,string"`
	Bio              string        `json:"bio"`
	CreatedAt        time.Time     `json:"created_at"`
	DisplayName      string        `json:"display_name"`
	Email            string        `json:"email"`
	EmailVerified    bool          `json:"email_verified"`
	Logo             *jsonURL      `json:"logo"`
	Name             string        `json:"name"`
	Notifications    Notifications `json:"notifications"`
	Partnered        bool          `json:"partnered"`
	TwitterConnected bool          `json:"twitter_connected"`
	Type             string        `json:"type"`
	UpdatedAt        time.Time     `json:"updated_at"`
}

type Notifications struct {
	Email bool `json:"email"`
	Push  bool `json:"push"`
}

// GetUsers gets the user information for the given user names.
func (c *Client) GetUsers(usernames []string) ([]*User, error) {
	request, err := http.NewRequest("GET", "users", nil)
	if err != nil {
		return nil, err
	}
	query := request.URL.Query()
	query.Set("login", strings.Join(usernames, ","))
	request.URL.RawQuery = query.Encode()
	response, err := (*http.Client)(c).Do(request)
	if err != nil {
		return nil, err
	}

	body, err := ioutil.ReadAll(response.Body)
	response.Body.Close()
	if err != nil {
		return nil, err
	}
	var users struct {
		Total int     `json:"_total"`
		Users []*User `json:"users"`
	}
	err = json.Unmarshal(body, &users)
	return users.Users, err
}
