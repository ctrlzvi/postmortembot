package twitch

import (
	"github.com/fluffle/goirc/client"
)

const host = "irc.chat.twitch.tv"

// ChatClient is a client for Twitch chat.
type ChatClient struct {
	irc *client.Conn
}

// Join joins the channel for the given broadcaster.
func (c *ChatClient) Join(broadcaster *Broadcaster) {
	c.irc.Join(broadcaster.Channel())
}

// Part leaves the channel for the given broadcaster.
func (c *ChatClient) Part(broadcaster *Broadcaster) {
	c.irc.Part(broadcaster.Channel())
}

// Connect connects to the Twitch chat server.
func (c *ChatClient) Connect() error {
	return c.irc.Connect()
}

// Message sends a message to the specified recipient (channel or user).
func (c *ChatClient) Message(target, message string) {
	c.irc.Privmsg(target, message)
}
