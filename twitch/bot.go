package twitch

import (
	"crypto/tls"
	"net/http"
	"regexp"
	"time"

	"github.com/fluffle/goirc/client"
	"github.com/fluffle/goirc/state"
	"github.com/gorilla/mux"
	"github.com/sirupsen/logrus"
	"gitlab.com/ctrlzvi/postmortembot/streamlabs/api/v1"
	"gitlab.com/ctrlzvi/postmortembot/twitch/api/v5"
)

// commandRegexp extracts a command from a chat message.
var commandRegexp = regexp.MustCompile(`^\s*!([^\s]+)(?:\s+(.*))?\s*$`)

// Bot is a Twitch bot.
type Bot struct {
	chat         *ChatClient
	twitch       *v5.Client
	server       *http.Server
	broadcasters *BroadcasterList
	commands     *CommandMap
}

// NewBot returns a new Bot.
func NewBot(user, token, clientID string) *Bot {
	router := mux.NewRouter()
	bot := &Bot{
		twitch: v5.NewClient(clientID),
		server: &http.Server{
			Handler: router,
			Addr:    "127.0.0.1:8000",
			// Good practice: enforce timeouts for servers you create!
			WriteTimeout: 15 * time.Second,
			ReadTimeout:  15 * time.Second,
		},
		broadcasters: NewBroadcasterList(),
		commands:     NewCommandMap(),
	}
	bot.chat = bot.ChatClient(user, token)
	return bot
}

// Router returns the router associated with the server.
func (b *Bot) Router() *mux.Router {
	return b.server.Handler.(*mux.Router)
}

// RegisterBroadcaster registers a broadcaster with the bot.
func (b *Bot) RegisterBroadcaster(user string, streamlabs *v1.Client) (*Broadcaster, error) {
	// TODO (zeffron 2017-04-01) Make this not tied to streamlabs
	broadcaster, err := NewBroadcaster(user, streamlabs, b.twitch, b.chat)
	if err != nil {
		return nil, err
	}
	b.broadcasters.AddBroadcaster(broadcaster)

	go broadcaster.Follow()

	return broadcaster, nil
}

// Run runs the bot.
func (b *Bot) Run() error {
	logrus.Infof("Starting bot")
	if err := b.chat.Connect(); err != nil {
		logrus.Errorf("Failed to connect to Twitch chat: %s", err)
		return err
	}

	if err := b.server.ListenAndServe(); err != nil {
		logrus.Errorf("Webserver encountered an error: %s", err)
	}

	return nil
}

// ChatClient returns a new ChatClient for the Bot.
func (b *Bot) ChatClient(user, password string) *ChatClient {
	config := &client.Config{
		Me:       &state.Nick{Nick: user},
		Server:   host,
		Pass:     password,
		PingFreq: 3 * time.Minute,
		Recover:  (*client.Conn).LogPanic,
		SplitLen: 450,
		SSL:      true,
		SSLConfig: &tls.Config{
			ServerName: host,
		},
		Timeout: 10 * time.Second,
	}

	joinHandler := client.HandlerFunc(func(conn *client.Conn, line *client.Line) {
		conn.Privmsg(line.Args[0], "Hello friends!")
	})

	irc := client.Client(config)
	irc.HandleBG(client.JOIN, joinHandler)
	irc.HandleBG(client.PRIVMSG, client.HandlerFunc(b.processCommand))

	return &ChatClient{
		irc: irc,
	}
}

// RegisterCommand registers a new command with the bot.
func (b *Bot) RegisterCommand(name string, handler CommandFunc) {
	b.commands.AddCommand(name, handler)
}

// processCommand checks a message for a command and calls the appropriate
// handler if there is one.
func (b *Bot) processCommand(conn *client.Conn, line *client.Line) {
	submatches := commandRegexp.FindStringSubmatch(line.Text())
	if submatches == nil {
		logrus.Errorf("Failed to match %s against the command regexp", line.Text())
		return
	}

	command, args := submatches[1], submatches[2]
	handler, ok := b.commands.Handler(command)
	if !ok {
		logrus.Warningf(`No handler found for command "%s"`, command)
		return
	}

	broadcaster := b.broadcasters.GetBroadcasterForChannel(line.Target())
	broadcaster.SendCommand(&Command{
		Name:      command,
		Arguments: args,
		User:      line.Nick,
		Timestamp: line.Time,
		Handler:   handler,
	})
}
