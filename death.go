package main

import (
	"time"

	"gitlab.com/ctrlzvi/postmortembot/event"
)

// DeathEventType is the type of the Death Event.
const (
	DeathEventType = "death"
	unknownCauses  = "unknown causes"
)

// Death is an Event representing a player death.
type Death struct {
	cause     string
	timestamp time.Time
}

// NewDeath returns a pointer to a new Death.
func NewDeath(cause string, timestamp time.Time) *Death {
	return &Death{
		cause:     cause,
		timestamp: timestamp,
	}
}

// Type returns the type of event when a Death is being used as an Event.
func (d *Death) Type() event.Type {
	return DeathEventType
}

// Note returns the associated note for the Death.
func (d *Death) Note() string {
	if d.cause == "" {
		return unknownCauses
	}
	return d.cause
}

// Timestamp returns the timestamp for the Death.
func (d *Death) Timestamp() time.Time {
	return d.timestamp
}

// Cause returns the cause of death.
func (d *Death) Cause() string {
	return d.cause
}

// UpdateCause updates the note for the Death.
func (d *Death) UpdateCause(cause string) {
	d.cause = cause
}
