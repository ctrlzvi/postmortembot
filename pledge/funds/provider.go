package funds

import (
	"net/url"
	"sync"
	"time"

	"github.com/shopspring/decimal"
)

// Provider represents a payment processor provider.
type Provider interface {
	Authorize(payee string, amount decimal.Decimal) (*url.URL, chan Payment, error)
}

// CSRFTokens is a collection of the currently active CSRF tokens for Oauth2
// authentication.
type CSRFTokens struct {
	sync.RWMutex
	tokens map[string]struct {
		timestamp time.Time
		state     interface{}
	}
}

// NewCSRFTokens creates a new collection of CSRFTokens.
func NewCSRFTokens() *CSRFTokens {
	return &CSRFTokens{
		RWMutex: sync.RWMutex{},
		tokens: map[string]struct {
			timestamp time.Time
			state     interface{}
		}{},
	}
}

// AddToken adds a new active CSRF token.
func (p *CSRFTokens) AddToken(paymentID string, state interface{}) {
	p.Lock()
	defer p.Unlock()
	p.tokens[paymentID] = struct {
		timestamp time.Time
		state     interface{}
	}{
		timestamp: time.Now(),
		state:     state,
	}
}

// ConsumeToken adds a new active CSRF token.
func (p *CSRFTokens) ConsumeToken(token string) (interface{}, bool) {
	p.Lock()
	defer p.Unlock()
	state, ok := p.tokens[token]
	delete(p.tokens, token)
	return state.state, ok && (time.Now().Sub(state.timestamp)) < 15*time.Minute
}
