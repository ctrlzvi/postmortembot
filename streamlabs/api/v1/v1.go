package v1

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"

	"github.com/shopspring/decimal"
	"github.com/sirupsen/logrus"
)

// Client represents a client for the Streamlabs API.
type Client http.Client

// User gets the user authenticated with the client.
func (c *Client) User() (user *User, err error) {
	response, err := (*http.Client)(c).Get("https://streamlabs.com/api/v1.0/user")
	if err != nil {
		logrus.Errorf("Error getting Streamlabs user: %s", err)
		return nil, err
	}
	body, err := ioutil.ReadAll(response.Body)
	response.Body.Close()
	if err != nil {
		logrus.Errorf("Error reading body from Streamlabs user: %s", err)
		return nil, err
	}
	err = json.Unmarshal(body, &user)
	if err != nil {
		logrus.Errorf("Error unmarshalling the body from Streamlabs: %s", err)
		return nil, err
	}
	return user, nil
}

// Donate creates a donation.
func (c *Client) Donate(donation *Donation) error {
	body, err := json.Marshal(donation)
	if err != nil {
		return err
	}
	_, err = (*http.Client)(c).Post(
		"https://streamlabs.com/api/v1.0/donations",
		"application/json",
		bytes.NewReader(body),
	)
	return err
}

// Decimal wraps decimal.Decimal for use in the Streamlabs API.
type Decimal decimal.Decimal

// MarshalJSON implements the json.Marshaler interface.
func (d *Decimal) MarshalJSON() ([]byte, error) {
	return []byte(fmt.Sprintf(`"%s"`, (*decimal.Decimal)(d).StringFixed(2))), nil
}

// User represents a StreamLabs user.
type User struct {
	TwitchUser `json:"twitch"`
}

// TwitchUser represents a StreamLabs user who streams with Twitch.
type TwitchUser struct {
	ID          int64  `json:"id"`
	DisplayName string `json:"display_name"`
	Name        string `json:"name"`
}

// Donation represents a donation.
type Donation struct {
	Name       string   `json:"name"`
	Message    string   `json:"message"`
	Identifier string   `json:"identifier"`
	Amount     Decimal  `json:"amount"`
	Currency   Currency `json:"currency"`
}

// Currency is the type used to represent a currency.
type Currency string

// USD is the string used for United States dollars.
const USD = Currency("USD")
