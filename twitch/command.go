package twitch

import (
	"sync"
	"time"
)

// Command represents a command from a user.
type Command struct {
	Name      string
	Arguments string
	User      string
	Timestamp time.Time
	Handler   CommandFunc
}

// CommandFunc is a function that processes a command.
type CommandFunc func(*Command, *Broadcaster)

// CommandMap is a thread safe map of command names to handlers.
type CommandMap struct {
	sync.RWMutex
	commands map[string]CommandFunc
}

// NewCommandMap returns a new CommandMap.
func NewCommandMap() *CommandMap {
	return &CommandMap{
		RWMutex:  sync.RWMutex{},
		commands: map[string]CommandFunc{},
	}
}

// AddCommand adds a command to the map.
func (cm *CommandMap) AddCommand(name string, handler CommandFunc) {
	cm.Lock()
	defer cm.Unlock()
	cm.commands[name] = handler
}

// Handler gets the handler for a given command.
func (cm *CommandMap) Handler(name string) (CommandFunc, bool) {
	cm.RLock()
	defer cm.RUnlock()
	handler, ok := cm.commands[name]
	return handler, ok
}
