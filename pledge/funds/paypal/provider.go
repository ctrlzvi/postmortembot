package paypal

import (
	"context"
	"net/http"
	"net/url"
	"sync"

	"github.com/gorilla/mux"
	"github.com/logpacker/PayPal-Go-SDK"
	"github.com/shopspring/decimal"
	"github.com/sirupsen/logrus"
	"gitlab.com/ctrlzvi/postmortembot/pledge/funds"
)

var (
	csrfTokens = funds.NewCSRFTokens()
	stateKey   = contextKey("state")
)

type contextKey string

// Provider represents the PayPal payment provider.
type Provider paypalsdk.Client

// NewProvider returns a new Provider.
func NewProvider(router *mux.Router, clientID, secret string) (*Provider, error) {
	client, err := paypalsdk.NewClient(clientID, secret, paypalsdk.APIBaseSandBox)
	if err != nil {
		logrus.Errorf("Failed to connect to the PayPal API: %s", err)
		return nil, err
	}
	_, err = client.GetAccessToken()
	if err != nil {
		logrus.Errorf("Failed to obtain a PayPal API access token: %s", err)
		return nil, err
	}

	router.Path("/funds/paypal/authorize").Queries("paymentId", "{paymentID}", "token", "{token}", "PayerID", "{payerID}").MatcherFunc(
		func(r *http.Request, rm *mux.RouteMatch) bool {
			state, ok := csrfTokens.ConsumeToken(r.URL.Query().Get("paymentId"))
			if !ok {
				return false
			}
			*r = *r.WithContext(context.WithValue(r.Context(), stateKey, state))
			return true
		},
	).HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		state := r.Context().Value(stateKey).(chan funds.Payment)
		defer close(state)
		payment, err := client.GetPayment(mux.Vars(r)["paymentID"])
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			w.Write([]byte(err.Error()))
			return
		}
		state <- &Payment{
			Mutex:    sync.Mutex{},
			payment:  payment,
			consumed: decimal.New(0, 0),
			client:   client,
		}
	})

	return (*Provider)(client), nil
}

// Authorize authorizes a payment through PayPal.
func (p *Provider) Authorize(payee string, amount decimal.Decimal) (*url.URL, chan funds.Payment, error) {
	payment, err := (*paypalsdk.Client)(p).CreatePayment(
		paypalsdk.Payment{
			Intent: "sale",
			Payer: &paypalsdk.Payer{
				PaymentMethod: "paypal",
			},
			Transactions: []paypalsdk.Transaction{{
				Amount: &paypalsdk.Amount{
					Currency: "USD",
					Total:    amount.String(),
				},
				Description: "My Payment",
				Payee: &paypalsdk.Payee{
					// TODO (zeffron 2017-04-09) Get the email address from the
					// payee.
					Email: "viz_skywalker-recipient@yahoo.com",
					PayeeDisplayMetadata: &paypalsdk.PayeeMetadataInfo{
						BrandName: payee,
					},
				},
			}},
			RedirectURLs: &paypalsdk.RedirectURLs{
				ReturnURL: "http://localhost:8000/api/v1/funds/paypal/authorize",
				CancelURL: "http://localhost:8000/api/v1/funds/paypal/cancel",
			},
		},
	)
	if err != nil {
		logrus.Errorf("Failed to create the payment: %s", err)
		return nil, nil, err
	}

	links := map[string]string{}
	for _, link := range payment.Links {
		links[link.Rel] = link.Href
	}
	authorizationURL, err := url.Parse(links["approval_url"])
	if err != nil {
		logrus.Errorf("Failed to parse PayPal approval URL: %s", err)
		return nil, nil, err
	}
	responseChannel := make(chan funds.Payment)
	csrfTokens.AddToken(payment.ID, responseChannel)
	return authorizationURL, responseChannel, nil
}
