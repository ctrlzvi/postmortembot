package event

import (
	"time"
)

// Type is the type used to identify events.
type Type string

// Event represents a discrete event.
type Event interface {
	Type() Type
	Note() string
	Timestamp() time.Time
}

// History tracks the history of events.
type History []Event

// NewHistory returns a pointer to a new History.
func NewHistory() *History {
	return &History{}
}

// AddEvent adds an event to the history.
func (h *History) AddEvent(event Event) {
	*h = append([]Event{event}, *h...)
}

// // MostRecentEventOfType gets the most recent event of the given type.
// func (eh *EventHistory) MostRecentEventOfType(eventType EventType) Event {
// 	events, ok := eh.events[eventType]
// 	if !ok {
// 		return nil
// 	}
// 	return events[0]
// }

// // EventsOfType gets the events of the given type.
// func (eh *EventHistory) EventsOfType(eventType EventType) []Event {
// 	return eh.events[eventType]
// }
