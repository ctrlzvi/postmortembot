package twitch

import (
	"fmt"
	"runtime"
	"strings"
	"sync"
	"time"

	"github.com/shopspring/decimal"
	"github.com/sirupsen/logrus"
	"gitlab.com/ctrlzvi/postmortembot/event"
	"gitlab.com/ctrlzvi/postmortembot/pledge"
	"gitlab.com/ctrlzvi/postmortembot/streamlabs/api/v1"
	"gitlab.com/ctrlzvi/postmortembot/twitch/api/v5"
)

// Broadcaster represents a registered broadcaster.
type Broadcaster struct {
	user       *v5.User
	streamlabs *v1.Client
	twitch     *v5.Client
	chat       *ChatClient
	events     map[event.Type]*event.History
	pledges    map[event.Type]*pledge.Collection
	done       chan struct{}
	commands   chan *Command
}

// NewBroadcaster returns a pointer to a new Broadcaster.
func NewBroadcaster(user string, streamlabs *v1.Client, twitch *v5.Client, chat *ChatClient) (*Broadcaster, error) {
	users, err := twitch.GetUsers([]string{user})
	if err != nil {
		return nil, err
	}
	return &Broadcaster{
		user:       users[0],
		streamlabs: streamlabs,
		twitch:     twitch,
		chat:       chat,
		events:     make(map[event.Type]*event.History),
		pledges:    make(map[event.Type]*pledge.Collection),
		done:       make(chan struct{}),
		commands:   make(chan *Command),
	}, nil
}

// Follow follows the broadcaster's stream.
// TODO (zeffron 2017-04-13) Find a better name.
func (b *Broadcaster) Follow() {
	defer func() {
		if err := recover(); err != nil {
			const size = 64 << 10
			buf := make([]byte, size)
			buf = buf[:runtime.Stack(buf, false)]
			logrus.Errorf("Panic for broacaster %s: %s\n%s", b.DisplayName(), err, buf)
		}
	}()
	streamStart := time.Time{}
	offlineTimeout := new(time.Timer)
	var session chan struct{}

	logrus.Infof("Checking %s's stream status", b.DisplayName())
	stream, err := b.twitch.GetStreamByUser(b.user, v5.Live)
	if err != nil {
		logrus.Errorf("Failed to get the stream for %s: %s", b.DisplayName(), err)
	}
	if stream != nil {
		streamStart = stream.CreatedAt
		session = b.session()
	}
	ticker := time.NewTicker(time.Minute)

Loop:
	for {
		select {
		// Broadcaster has unregistered
		case <-b.done:
			logrus.Infof("Finished the follow loop")
			if session != nil {
				session <- struct{}{}
				<-session
			}
			ticker.Stop()
			break Loop

		// Poll the stream for status once per minute
		case <-ticker.C:
			logrus.Infof("Checking %s's stream status", b.DisplayName())
			// Check the status of the stream once per minute
			stream, err := b.twitch.GetStreamByUser(b.user, v5.Live)
			if err != nil {
				logrus.Errorf("Failed to get %s's stream: %s", b.DisplayName(), err)
				continue
			}

			switch {
			case stream == nil:
				logrus.Infof("%s's stream is offline.", b.DisplayName())
				if session != nil && offlineTimeout.C == nil {
					// const timeout = 5 * time.Minute
					const timeout = 1 * time.Second
					logrus.Infof("%s's stream might be offline. Waiting %d minutes to see if the stream comes back.", b.DisplayName(), int(timeout/time.Minute))
					offlineTimeout = time.NewTimer(timeout)
				}
			case stream.CreatedAt.After(streamStart):
				logrus.Infof("%s's stream has started.", b.DisplayName())
				streamStart = stream.CreatedAt
				if offlineTimeout.C != nil {
					logrus.Infof("%s's stream is online. No longer waiting.", b.DisplayName())
					if !offlineTimeout.Stop() {
						<-offlineTimeout.C
					}
					offlineTimeout = new(time.Timer)
				}
				if session != nil {
					if offlineTimeout.C == nil {
						logrus.Warningf("%s has started a new stream, but the old one wasn't finished: %#v", b.DisplayName(), stream)
					}
					session <- struct{}{}
					<-session
				}
				session = b.session()
			case stream.CreatedAt.Equal(streamStart):
				logrus.Infof("%s's stream is still online.", b.DisplayName())
				if offlineTimeout.C != nil {
					logrus.Infof("%s's stream is online. No longer waiting.", b.DisplayName())
					if !offlineTimeout.Stop() {
						<-offlineTimeout.C
					}
					offlineTimeout = new(time.Timer)
				} else if session == nil {
					logrus.Warningf("%s is continuing a stream that we thought was finished. Creating a new session for it.")
					// TODO (zeffron 2017-04-14) Proper messaging to the channel about lost pledges.
					session = b.session()
				}
			}

		// Timeout on the stream going offlinr
		case <-offlineTimeout.C:
			logrus.Infof("%s's stream has ended.", b.DisplayName())
			session <- struct{}{}
			<-session
			session = nil
			offlineTimeout = new(time.Timer)
		}
	}
}

// session tracks pledges during a single streamed session.
func (b *Broadcaster) session() chan struct{} {
	done := make(chan struct{})
	go func() {
		b.events = make(map[event.Type]*event.History)
		b.pledges = make(map[event.Type]*pledge.Collection)

		logrus.Infof("Joining channel %s", b.Channel())
		b.chat.Join(b)

		for {
			select {
			case command := <-b.commands:
				b.processCommand(command)
			case <-done:
				defer close(done)

				// FIXME (zeffron 2017-04-15) Races with accepting the command from the chat system
			DrainChannel:
				for {
					select {
					case command := <-b.commands:
						b.processCommand(command)
					default:
						break DrainChannel
					}
				}

				donations := decimal.New(0, 0)
				wg := sync.WaitGroup{}
				for _, pledges := range b.pledges {
					wg.Add(len(*pledges))
					for p := range *pledges {
						p := p
						go func() {
							p.Finalize()
							wg.Done()
						}()
					}
					donations = donations.Add(pledges.Donations())
				}
				b.messageChannel(fmt.Sprintf("Thanks to your support, %s raised $%s this session for their funeral.", b.DisplayName(), donations.StringFixed(2)))
				logrus.Infof("Leaving channel %s", b.Channel())
				b.chat.Part(b)
				wg.Wait()

				return
			}
		}
	}()
	return done
}

// processCommand processes the given command.
func (b *Broadcaster) processCommand(command *Command) {
	defer func() {
		if err := recover(); err != nil {
			const size = 64 << 10
			buf := make([]byte, size)
			buf = buf[:runtime.Stack(buf, false)]
			logrus.Errorf("Panic for command %s: %s\n%s", command.Name, err, buf)
		}
	}()
	command.Handler(command, b)
}

// SendCommand sends a command to the broadcaster for processing.
func (b *Broadcaster) SendCommand(command *Command) {
	b.commands <- command
}

// DisplayName gets the display name of the broadcaster.
func (b *Broadcaster) DisplayName() string {
	return b.user.DisplayName
}

// Channel gets the channel for the broadcaster.
func (b *Broadcaster) Channel() string {
	return fmt.Sprintf("#%s", b.user.Name)
}

// messageChannel sends a chat message to the Broadcaster's channel.
func (b *Broadcaster) messageChannel(message string) {
	b.chat.Message(b.Channel(), message)
}

// Reply replies to the given user in the broadcaster's channel.
func (b *Broadcaster) Reply(user, message string) {
	response := fmt.Sprintf("@%s %s", user, message)
	b.messageChannel(response)
}

// PrivateReply replies to the user via a whisper.
func (b *Broadcaster) PrivateReply(user, message string) {
	response := fmt.Sprintf(".w %s %s", user, message)
	b.messageChannel(response)
}

// AddEvent adds an event to the Broadcaster's history.
func (b *Broadcaster) AddEvent(e event.Event) {
	history, ok := b.events[e.Type()]
	if !ok {
		history = event.NewHistory()
		b.events[e.Type()] = history
	}
	history.AddEvent(e)
}

// LastEvent gets the most recent event of the specified type.
func (b *Broadcaster) LastEvent(t event.Type) event.Event {
	history, ok := b.events[t]
	if !ok {
		return nil
	}
	return (*history)[0]
}

// History returns (a copy of) the event history for the specified event type.
func (b *Broadcaster) History(t event.Type) event.History {
	history, ok := b.events[t]
	if !ok {
		return *event.NewHistory()
	}
	return *history
}

// AddPledge adds a pledge to the broadcaster for the specified event type.
func (b *Broadcaster) AddPledge(t event.Type, p *pledge.Pledge) {
	collection, ok := b.pledges[t]
	if !ok {
		collection = pledge.NewCollection()
		b.pledges[t] = collection
	}
	collection.AddPledge(p)
}

// RemovePledge removes a pledge from the broadcaster for the specified event
// type.
func (b *Broadcaster) RemovePledge(t event.Type, p *pledge.Pledge) {
	collection, ok := b.pledges[t]
	if ok {
		collection.RemovePledge(p)
	}
}

// Pledges returns the pledges for the specified event type.
func (b *Broadcaster) Pledges(t event.Type) []*pledge.Pledge {
	collection, ok := b.pledges[t]
	if !ok {
		return nil
	}
	pledges := make([]*pledge.Pledge, 0, len(*collection))
	for p := range *collection {
		pledges = append(pledges, p)
	}
	return pledges
}

// Donate sends a donation to Streamlabs.
func (b *Broadcaster) Donate(amount decimal.Decimal, donors []string) error {
	donation := &v1.Donation{
		Identifier: "PostMortemBot",
		Amount:     v1.Decimal(amount),
		Currency:   v1.USD,
	}
	switch len(donors) {
	case 0:
		return nil
	case 1:
		donation.Name = donors[0]
		donation.Message = "You died!"
	case 2:
		// TODO (zeffron 2017-04-03) Once the streamer stops broadcasting,
		// replace this donation with individual donations (using delete,
		// create, and edit).
		donation.Name = "PostMortemBot"
		donation.Message = fmt.Sprintf("You died! (Donated by %s and %s)", donors[0], donors[1])
	default:
		// TODO (zeffron 2017-04-03) Once the streamer stops broadcasting,
		// replace this donation with individual donations (using delete,
		// create, and edit).
		donation.Name = "PostMortemBot"
		donation.Message = fmt.Sprintf("You died! (Donated by %s, and %s)", strings.Join(donors[0:len(donors)-1], ", "), donors[len(donors)-1])
	}
	err := b.streamlabs.Donate(donation)
	if err != nil {
		return err
	}
	return nil
}

// BroadcasterList is a thread safe list of broadcasters.
type BroadcasterList struct {
	sync.RWMutex
	broadcasters map[string]*Broadcaster
}

// NewBroadcasterList returns a new *BroadcasterList
func NewBroadcasterList() *BroadcasterList {
	return &BroadcasterList{
		RWMutex:      sync.RWMutex{},
		broadcasters: map[string]*Broadcaster{},
	}
}

// AddBroadcaster adds a broadcaster to the list.
func (bl *BroadcasterList) AddBroadcaster(broadcaster *Broadcaster) {
	bl.Lock()
	defer bl.Unlock()
	bl.broadcasters[broadcaster.user.Name] = broadcaster
}

// GetBroadcasterForChannel gets the broadcaster for a given channel.
func (bl *BroadcasterList) GetBroadcasterForChannel(channel string) *Broadcaster {
	bl.RLock()
	defer bl.RUnlock()
	return bl.broadcasters[strings.TrimPrefix(channel, "#")]
}
