package v5

import (
	"net/http"
	"net/url"
)

// Client represents a client to the Twitch API.
type Client http.Client

// NewClient returns a pointer to a new Client.
func NewClient(clientID string) *Client {
	return (*Client)(&http.Client{
		Transport: &transport{
			base:     http.DefaultTransport,
			clientID: clientID,
		},
	})
}

type jsonURL url.URL

func (j *jsonURL) UnmarshalJSON(data []byte) error {
	u, err := (url.Parse(string(data[1 : len(data)-1])))
	*j = jsonURL(*u)
	return err
}
