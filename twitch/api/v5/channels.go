package v5

import "time"

// Channel represents a Twitch channel.
type Channel struct {
	Mature                       bool      `json:"mature"`
	Status                       string    `json:"status"`
	BroadcasterLanguage          string    `json:"broadcaster_language"`
	DisplayName                  string    `json:"display_name"`
	Game                         string    `json:"game"`
	Language                     string    `json:"language"`
	ID                           int64     `json:"_id"`
	Name                         string    `json:"name"`
	CreatedAt                    time.Time `json:"created_at"`
	UpdateAt                     time.Time `json:"updated_at"`
	Partner                      bool      `json:"partner"`
	Logo                         *jsonURL  `json:"logo"`
	VideoBanner                  *jsonURL  `json:"video_banner"`
	ProfileBanner                *jsonURL  `json:"profile_banner"`
	ProfileBannerBackgroundColor string    `json:"profile_banner_background_color"`
	URL                          *jsonURL  `json:"url"`
	Views                        int64     `json:"views"`
	Followers                    int       `json:"followers"`
}
