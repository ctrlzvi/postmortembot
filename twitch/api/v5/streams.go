package v5

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"time"
)

const (
	// Live is the stream type to get the live stream.
	Live StreamType = "live"
	// Playlist is the stream type to get the playlist.
	Playlist StreamType = "playlist"
	// All is the stream type to use to get any type of stream.
	All StreamType = "all"
)

// StreamType is a wrapper to isolate the allowable stream types.
type StreamType string

// Stream represents a Twitch stream.
type Stream struct {
	ID          int64     `json:"_id"`
	Game        string    `json:"game"`
	Viewers     int       `json:"viewers"`
	VideoHeight int       `json:"video_height"`
	AverageFPS  int       `json:"average_fps"`
	Delay       int       `json:"delay"`
	CreatedAt   time.Time `json:"created_at"`
	IsPlaylist  bool      `json:"is_playlist"`
	Preview     struct {
		Small    *jsonURL `json:"small"`
		Medium   *jsonURL `json:"medium"`
		Large    *jsonURL `json:"large"`
		Template *jsonURL `json:"template"`
	} `json:"preview"`
	Channel *Channel `json:"channel"`
}

// GetStreamByUser gets the stream for the given user.
func (c *Client) GetStreamByUser(user *User, streamType StreamType) (*Stream, error) {
	request, err := http.NewRequest("GET", fmt.Sprintf("streams/%d", user.ID), nil)
	if err != nil {
		return nil, err
	}
	query := request.URL.Query()
	query.Set("stream_type", string(streamType))
	request.URL.RawQuery = query.Encode()
	response, err := (*http.Client)(c).Do(request)
	if err != nil {
		return nil, err
	}

	body, err := ioutil.ReadAll(response.Body)
	response.Body.Close()
	if err != nil {
		return nil, err
	}
	var stream struct {
		Stream *Stream `json:"stream"`
	}
	err = json.Unmarshal(body, &stream)
	return stream.Stream, err
}
