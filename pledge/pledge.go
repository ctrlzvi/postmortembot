package pledge

import (
	"fmt"
	"net/url"

	"gitlab.com/ctrlzvi/postmortembot/pledge/funds"

	"github.com/shopspring/decimal"
)

// Pledge represents a pledge.
type Pledge struct {
	donor     string
	recipient string
	amount    decimal.Decimal
	maximum   decimal.Decimal
	payment   funds.Payment
}

// NewPledge returns a pointer to a new Pledge.
func NewPledge(recipient, donor string, amount, maximum decimal.Decimal) *Pledge {
	return &Pledge{
		donor:     donor,
		recipient: recipient,
		amount:    amount,
		maximum:   maximum,
	}
}

// Donor returns the donor of the pledge.
func (p *Pledge) Donor() string {
	return p.donor
}

// Amount returns the amount of the pledge.
func (p *Pledge) Amount() decimal.Decimal {
	return p.amount
}

// Donated returns the amount actually donated via the pledge.
func (p *Pledge) Donated() decimal.Decimal {
	return p.payment.Amount()
}

// Consume consumes one pledge's worth of money towards the maximum.
// TODO (Zeffron 2017-04-03) Find a better name.
func (p *Pledge) Consume() error {
	if p.payment == nil {
		return fmt.Errorf("Pledge is not yet funded")
	}
	return p.payment.Capture(p.amount)
}

// Fund funds a pledge.
func (p *Pledge) Fund(provider funds.Provider) (*url.URL, error) {
	authorizationURL, channel, err := provider.Authorize(p.recipient, p.maximum)
	if err != nil {
		return nil, err
	}

	// FIXME (zeffron 2017-04-09) Add a timeout to stop leaking the goroutine
	// (use a context?) and clean up all resources when the timeout is
	// exceeded. This includes the CSRF token and the pledge itself.
	go func() {
		p.payment = <-channel
	}()
	return authorizationURL, nil
}

// Finalize finishes the pledge by defunding it and returns how many funds were
// left.
func (p *Pledge) Finalize() error {
	return p.payment.Finalize()
}

// Collection is a collection of pledges.
type Collection map[*Pledge]struct{}

// NewCollection returns a pointer to a new Collection.
func NewCollection() *Collection {
	return &Collection{}
}

// AddPledge adds the given pledge for the specified event type.
func (c *Collection) AddPledge(pledge *Pledge) {
	(*c)[pledge] = struct{}{}
}

// RemovePledge removes the given pledge from the specified event type.
func (c *Collection) RemovePledge(pledge *Pledge) {
	delete(*c, pledge)
}

// Donations returns the total amount donated through the collection.
func (c *Collection) Donations() decimal.Decimal {
	donations := decimal.New(0, 0)
	for pledge := range *c {
		donations = donations.Add(pledge.Donated())
	}
	return donations
}
