package streamlabs

import (
	"context"
	"net"
	"sync"
	"time"

	"net/http"

	"github.com/gorilla/mux"
	uuid "github.com/satori/go.uuid"
	"github.com/sirupsen/logrus"
	"golang.org/x/oauth2"
)

var streamlabsEndpoint = oauth2.Endpoint{
	AuthURL:  "https://streamlabs.com/api/v1.0/authorize",
	TokenURL: "https://streamlabs.com/api/v1.0/token",
}

// Oauth2CSRFTokens is a collection of the currently active CSRF tokens for
// Oauth2 authentication.
type Oauth2CSRFTokens struct {
	sync.Mutex
	tokens map[string]time.Time
}

// NewOauth2CSRFTokens creates a new collection of CSRFTokens.
func NewOauth2CSRFTokens() *Oauth2CSRFTokens {
	return &Oauth2CSRFTokens{
		Mutex:  sync.Mutex{},
		tokens: map[string]time.Time{},
	}
}

// CreateToken adds a new active CSRF token.
func (o *Oauth2CSRFTokens) CreateToken() string {
	o.Lock()
	defer o.Unlock()
	token := uuid.NewV4().String()
	o.tokens[token] = time.Now()
	return token
}

// ConsumeToken adds a new active CSRF token.
func (o *Oauth2CSRFTokens) ConsumeToken(token string) bool {
	o.Lock()
	defer o.Unlock()
	timestamp, ok := o.tokens[token]
	delete(o.tokens, token)
	return ok && (time.Now().Sub(timestamp)) < 15*time.Minute
}

// Authorize initiates the Oauth2 exchange (currently web server app flow)
// TODO (zeffron 2017-04-01) Switch to the single page app flow
func Authorize(ctx context.Context, route *mux.Route, tokens *Oauth2CSRFTokens) (string, error) {
	config, err := Config(ctx, route)
	if err != nil {
		logrus.Errorf("Failed to get the Oauth2 configuration for Streamlabs: %s", err)
		return "", err
	}
	return config.AuthCodeURL(tokens.CreateToken(), oauth2.AccessTypeOffline), nil
}

// Config gets an *oauth2.Config that will connect to Streamlabs
func Config(ctx context.Context, route *mux.Route) (*oauth2.Config, error) {
	host := ctx.Value(http.LocalAddrContextKey).(*net.TCPAddr).String()
	url, err := route.URL("host", host)
	if err != nil {
		logrus.Errorf("Could not build the URL for the authorization endpoint: %s", err)
		return nil, err
	}
	return &oauth2.Config{
		ClientID:     "",
		ClientSecret: "",
		Scopes:       []string{"donations.create"},
		Endpoint:     streamlabsEndpoint,
		RedirectURL:  url.String(),
	}, nil
}

// Token gets an Oauth2 token from an access code.
func Token(ctx context.Context, route *mux.Route, code string) (*oauth2.Token, error) {
	config, err := Config(ctx, route)
	if err != nil {
		logrus.Errorf("Failed to get the Oauth2 configuration for Streamlabs: %s", err)
		return nil, err
	}
	token, err := config.Exchange(ctx, code)
	if err != nil {
		logrus.Errorf("Failed to exchange the Streamlabs access code for a token: %s", err)
		return nil, err
	}
	return token, nil
}
